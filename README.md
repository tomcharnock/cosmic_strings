# Cosmic Strings
Code to do with Cosmic Strings.

## String Realisations
Highly efficient vectorised string realisation code in python can be found in `string_realisations/`

## cmbact4
A copy of `cmbact4` is here for comparisons. It has been editted to output the UETC of the first realisation and then stop. 
