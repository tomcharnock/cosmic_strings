import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as g

params = {'text.usetex': False, 'mathtext.fontset': 'stixsans','figure.figsize':[6,4.5],'legend.fontsize':8,'font.size':8}
plt.rcParams.update(params)

fig = plt.figure(figsize=(8,4.5))
gs = g.GridSpec(2,3)
gs.update(left=0.12,right=0.95,top=0.95,bottom=0.12,wspace=0.5,hspace=0.3)

tau = np.loadtxt('output/tau.dat')
ss00 = np.loadtxt('output/SS00.dat')
ss = np.loadtxt('output/SS.dat')
vv = np.loadtxt('output/VV.dat')
tt = np.loadtxt('output/TT.dat')
ss00cross = np.loadtxt('output/SS00CROSS.dat')

data = [ss00, ss, vv, tt, ss00cross]

j = 0
k = 0
for i in xrange(5):
	ax = plt.subplot(gs[k,j])
	c = ax.contourf(np.log10(tau),np.log10(tau),data[i])
	plt.colorbar(c, ax = ax, pad = 0.)
	plt.xticks([min(np.log10(tau)), (abs(max(np.log10(tau)))-abs(min(np.log10(tau))))/3., max(np.log10(tau))])
	ax.set_xlabel('$\log k\\tau$',labelpad=0.)
	ax.set_ylabel('$\log k\\tau$',labelpad=0.)
	j += 1
	if j == 3:
		k += 1
		j = 0

plt.show()
