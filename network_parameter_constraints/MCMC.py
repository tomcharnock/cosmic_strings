#import numpy as np


#load = np.loadtxt('spectra/cl_tt.d')
#l = load[:, 0]
#P_obs = load[:, 1:]

#gmu = 2.07e-6

#alpha = np.random.uniform(0, 10)
#cr = np.random.uniform(0, 1)


import numpy as np
import sys

class string_MCMC():
	def __init__(self, parameters):
		self.load_parameters(parameters)
		if self.create:
			self.MCMC()
		if self.plot_root != None:
			self.plot()
	
	def load_parameters(self, parameters):
		arguments = {
		'num_cls': 0,
		'P_obs_source': 'spectra/cl_tt.d',
		'params': ['cr', 'alpha'],
		'camb_root': 'camb_uetc',			
		'output_root': 'spectra',
		'submission_root': 'params', 
		'plot_root': None,
		'save_root': None, 
		'filename': None, 
		'plot_show': False, 
		'create': True,
		'load_cls': None, 
		'mcmc_chain': 'chains',
		'cr_mean': 0.23,
		'cr_std' : 0.1,
		'cr_lower': 0,
		'cr_upper': 1,
		'alpha_mean': 1,
		'alpha_std': 3,
		'alpha_lower': 0,
		'alpha_upper': 10,
		}					 
		for keys in parameters:
			arguments[keys] = parameters[keys]				 
		self.num_cls = arguments['num_cls']					 
		self.P_obs_source = arguments['P_obs_source']					
		self.P_obs = np.loadtxt(self.P_obs_source)
		self.num_ls = self.P_obs.shape[0]
		self.params = arguments['params']
		self.num_params = len(self.params)
		self.camb_root = arguments['camb_root']
		self.output_root = arguments['output_root']
		self.submission_root = arguments['submission_root']
		self.plot_root = arguments['plot_root']
		self.save_root = arguments['save_root']
		self.filename = arguments['filename']
		if self.filename == None and self.save_root != None:
			print "parameters['filename'] must be set"
			sys.exit() 
		if self.filename == None and self.plot_root != None:
			print "parameters['filename'] must be set" 
			sys.exit() 
		self.plot_show = arguments['plot_show']
		self.create = arguments['create']
		self.load_cls = arguments['load_cls']
		self.mcmc_chain = arguments['mcmc_chain']
		self.chain = None
		self.write_chain = None
		self.cr_mean = arguments['cr_mean']
		self.cr_std = arguments['cr_std']
		self.cr_lower = arguments['cr_lower']
		self.cr_upper = arguments['cr_upper']
		self.alpha_mean = arguments['alpha_mean']
		self.alpha_std = arguments['alpha_std']
		self.alpha_lower = arguments['alpha_lower']
		self.means = np.array([self.cr_mean, self.alpha_mean])
		self.cov = np.array([[self.cr_std**2., 0.], [0., self.alpha_std**2.]])
		self.alpha_upper = arguments['alpha_upper']
		self.back_root = '../'
		for num_back in xrange(self.camb_root.count('/')):
			self.back_root += '../'

	def submission(self, filenum, cr, alpha):
		import subprocess
		replacements = {'output_root': 'output_root = '+self.back_root+self.output_root+'/'+str(filenum), 'cr': 'cr = '+str(cr), 'alpha': 'alpha = '+str(alpha)}
		with open('params/params_string.ini', 'r') as infile, open(self.submission_root+'/params_'+str(filenum)+'.ini', 'w') as outfile:
			for line in infile:
				for src, target in replacements.iteritems():
					line = line.replace(src, target)
				outfile.write(line)
		p = subprocess.Popen('./camb '+self.back_root+self.submission_root+'/params_'+str(filenum)+'.ini > '+self.back_root+self.output_root+'/'+str(filenum)+'.outfile', cwd = self.camb_root, shell = True)
		p.communicate()
		if 'Done strings' in subprocess.check_output(['tail', '-1', self.output_root+'/'+str(filenum)+'.outfile']):
			failed = False
		else:
			failed = True

		return failed

	def create_cls(self):
		#if self.num_cls == 0:
		#	cl = 0
		#	while(1):
		#		cr, alpha = self.MCMC(cl)
		#		cl += 1
		#else:
		for cl in xrange(self.num_cls):
			self.submission(cl)

	def like(self, filenum):
		P_theory = np.empty([self.num_ls, 3])
		P_theory[:, 0] = np.loadtxt(self.output_root+'/'+str(filenum)+'_scalCls.dat')[:, 1]
		P_theory[:, 1] = np.loadtxt(self.output_root+'/'+str(filenum)+'_vecCls.dat')[:, 1]
		P_theory[:, 2] = np.loadtxt(self.output_root+'/'+str(filenum)+'_tensCls.dat')[:, 1]
		return np.sum(-(self.P_obs[:, 1]-P_theory[:, 0])**2.)+np.sum(-(self.P_obs[:, 2]-P_theory[:, 1])**2.)+np.sum(-(self.P_obs[:, 3]-P_theory[:, 2])**2.)


	def draw_parameters(self, parameters = None):
		if parameters is None:
			parameters = np.random.multivariate_normal(self.means, self.cov)
			if (parameters[0] >= self.cr_lower) and (parameters[0] <= self.cr_upper) and (parameters[1] >= self.alpha_lower) and (parameters[1] <= self.alpha_upper):
				return self.draw_parameters(parameters = parameters)
			else:
				return self.draw_parameters(parameters = None)
		else:
			return parameters

	def MCMC(self):
		if self.num_cls == 0:
			cl = 0
			while self.chain is None:
				parameters = self.draw_parameters()
				failed = self.submission(cl, parameters[0], parameters[1])
				if failed:
					likelihood = np.nan
					if self.write_chain is None:
						self.write_chain = [[np.nan, parameters[0], parameters[1]]]
					else:
						self.write_chain.append([np.nan, parameters[0], parameters[1]])
				else:
					likelihood = self.like(cl)
					if self.write_chain is None:
						self.write_chain = [[likelihood, parameters[0], parameters[1]]]
					else:
						self.write_chain.append([likelihood, parameters[0], parameters[1]])
					self.chain = [[likelihood, parameters[0], parameters[1]]]
					np.save(self.mcmc_chain + '/fail_chain.npy', self.write_chain)
					np.save(self.mcmc_chain + '/chain.npy', self.chain)
					cl += 1
			while(1):
				parameters = self.draw_parameters()
				print cl, parameters
				failed = self.submission(cl, parameters[0], parameters[1])				
				if failed:
					likelihood = np.nan
					self.write_chain.append([np.nan, parameters[0], parameters[1]])
				else:
					likelihood = self.like(cl)
					self.write_chain.append([likelihood, parameters[0], parameters[1]])
					if likelihood/self.chain[-1][0] <= 1:
						self.write_chain.append([likelihood, parameters[0], parameters[1]])
						self.chain.append([likelihood, parameters[0], parameters[1]])
					else:
						random_num = np.random.uniform(0, 1)
						if likelihood/self.chain[-1][0] <= random_num:
							self.write_chain.append([likelihood, parameters[0], parameters[1]])
							self.chain.append([likelihood, parameters[0], parameters[1]])
						else:
							self.write_chain.append([likelihood, parameters[0], parameters[1]])
							self.chain.append(self.chain[-1])
					np.save(self.mcmc_chain + '/fail_chain.npy', self.write_chain)
					np.save(self.mcmc_chain + '/chain.npy', self.chain)
					cl += 1
				if cl%100 == 0:
					self.means = np.mean(np.array(self.chain)[:, 1:], axis = 0)
					self.cov = np.cov(np.array(self.chain)[:, 1:], rowvar = False)


	'''
	#load cls from file if already made
	def load_data(self):
		import random
		if self.load_cls == None:
			cls = np.empty([self.num_cls, self.num_ls, 3])
			parameter_values = np.empty([self.num_cls, self.num_params])
			for cl in xrange(self.num_cls):
				cls[cl, :, 0] = np.loadtxt(self.output_root+'/'+str(cl)+'_scal.dat')[:self.num_ls, 1]
				cls[cl, :, 1] = np.loadtxt(self.output_root+'/'+str(cl)+'_vec.dat')[:self.num_ls, 1]
				cls[cl, :, 2] = np.loadtxt(self.output_root+'/'+str(cl)+'_ten.dat')[:self.num_ls, 1]
				with open(self.output_root+'/'+str(cl)+'_params.ini') as parameters:
					for line in parameters:
						param_values = line.split()
						if param_values[0] in self.params:
							parameter_values[cl, self.params.index(param_values[0])] = param_values[-1]
	'''

	def plot(self):
		import matplotlib.pyplot as plt
		import matplotlib.gridspec as g
		params = {'text.usetex': False, 'mathtext.fontset': 'stixsans','legend.fontsize':6,'font.size':6}
		plt.rcParams.update(params)

		fig = plt.figure(figsize = (6, 1.5))
		gs = g.GridSpec(1, 3)
		gs.update(left=0.07, right=0.98, top=0.95, bottom = 0.18, wspace=0.5, hspace=0)
	
		labels = ['$C^{S}_{l}', '$C^{V}_{l}', '$C^{T}_{l}']
		svt = ['scal', 'vec', 'ten']
		for num in xrange(self.num_cls):
			for i in xrange(3):
				data = np.loadtxt(self.output_root+'/'+str(num)+'_'+str(svt[i])+'.dat')
				ax = plt.subplot(gs[i])
				ax.plot(data[:,0],data[:,i+1], color = 'black', alpha = 0.1)
				ax.set_xscale('log')
				ax.set_xlabel('$l$',labelpad=0)
				ax.set_ylabel(labels[i]+'l(l+1)/2\pi[\mu K^2]$',labelpad=0)
		plt.savefig(self.plot_root+'/'+self.filename+'_cls.pdf')
		if self.plot_show:
			plt.show()
		plt.close()

if __name__ == "__main__":
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-file', type = str, default = 'params')
	sys.path.append('params/')
	params = __import__(parser.parse_args().file)
	string_MCMC(params.parameters)




