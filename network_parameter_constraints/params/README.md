# Directory where parameter files are stored
`params.py` contains the dictionary of parameters for `MCMC.py`.
`params_string.ini` contains the `camb_uetc/camb` parameters (this is copied for different cr and alpha values).
`params_i.ini` are created where `i` are working parameter files which correspond with spectra in the `spectra/` directory.
