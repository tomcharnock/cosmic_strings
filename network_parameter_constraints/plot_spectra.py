import numpy as np
import matplotlib.pyplot as plt

def likelihood(gmu):
	l = np.float128(NG[:, 0])
	Phat_scal = np.float128(NG[:, 1])#/(l*(l+np.float128(1.)))
	P_scal = np.float128(spec_scal[:, 1]*gmu**np.float128(2.))#/(l*(l+np.float128(1.)))
	Phat_vec = np.float128(NG[:, 2])#/(l*(l+np.float128(1.)))
	P_vec = np.float128(spec_vec[:, 1]*gmu**np.float128(2.))#/(l*(l+np.float128(1.)))
	Phat_ten = np.float128(NG[:, 3])#/(l*(l+np.float128(1.)))
	P_ten = np.float128(spec_ten[:, 1]*gmu**np.float128(2.))#/(l*(l+np.float128(1.)))
	return np.sum(-((Phat_scal-P_scal)**np.float128(2.)))+np.sum(-((Phat_vec-P_vec)**np.float128(2.)))+np.sum(-((Phat_ten-P_ten)**np.float128(2.)))
        #return np.sum(np.log(np.sqrt((np.float128(2.)*l+np.float128(1.))/(np.float128(2.)*P**np.float128(2.))))*((np.float128(-1.)*(np.float128(2.)*l+np.float128(1.))*(Phat-P)**np.float128(2.))/(np.float128(4.)*P**np.float128(2.))))
        #return np.sum((np.float128(-1.)*(np.float128(2.)*l+np.float128(1.))*(Phat-P)**np.float128(2.))/(np.float128(4.)*P**np.float128(2.)))

NG = np.loadtxt('spectra/cl_tt.d')
spec_tot = np.loadtxt('spectra/cl_tt_tot.dat')
spec_scal = np.loadtxt('spectra/cl_tt_scal.dat')
spec_vec = np.loadtxt('spectra/cl_tt_vec.dat')
spec_ten = np.loadtxt('spectra/cl_tt_ten.dat')

gmu_array = []
like_array = []
gmu = np.float128(1.1e-6)
like = likelihood(gmu)
for i in xrange(int(1e5)):
	rand = np.float128(np.random.uniform(0,10))
	new_gmu = gmu*rand
	new_like = likelihood(new_gmu)
	if not np.isnan(new_like): 
		if new_like > like:
			print new_like, new_gmu
			gmu_array.append(new_gmu)
			gmu = new_gmu
			like_array.append(new_like)
			like = new_like
		

like_array = like_array/np.max(np.abs(like_array))
fake_like = 1.+like_array

fig = plt.figure()
ax1 = fig.add_subplot(131)
ax2 = fig.add_subplot(132)
ax3 = fig.add_subplot(133)

for i in xrange(len(gmu_array)):
	ax1.plot(spec_scal[:, 0], spec_scal[:, 1]*gmu_array[i]**2, alpha = fake_like[i], color = 'green')
	ax2.plot(spec_vec[:, 0], spec_vec[:, 1]*gmu_array[i]**2, alpha = fake_like[i], color = 'green')
	ax3.plot(spec_ten[:, 0], spec_ten[:, 1]*gmu_array[i]**2, alpha = fake_like[i], color = 'green')


ax1.plot(NG[:, 0], NG[:, 1], color = 'blue')
ax1.plot(spec_scal[:, 0], spec_scal[:, 1], color = 'black')
ax1.set_xscale('log')
#ax1.set_yscale('log')

ax2.plot(NG[:, 0], NG[:, 2], color = 'blue')
ax2.plot(spec_vec[:, 0], spec_vec[:, 1], color = 'black')
ax2.set_xscale('log')
#ax2.set_yscale('log')

ax3.plot(NG[:, 0], NG[:, 3], color = 'blue')
ax3.plot(spec_ten[:, 0], spec_ten[:, 1], color = 'black')
ax3.set_xscale('log')
#ax3.set_yscale('log')

plt.show()
