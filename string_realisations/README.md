# Vectorised String Realisation Code in Python

## Evolution only
```
import string_realisations as SR
E = SR.Evolution()
E.evolution_plot()
```

## UETC
To do 10000 realisations, considering a universe with 25% Baryonic matter and 75% CDM only (note that using 1000 realisations seems to be much faster than 10000 so it's probably worth looping over 1000 realisations 10 times to get the same effect)
```
import string_realisations as SR
S = SR.Strings(C = SR.Constants(Omega_b = 0.25, Omega_c = 0.75, Omega_v = 0, Neff = 0.), S = SR.StringParameters(num_rel = 10000))
S.uetc(50, 1, 10**-2, 10**2)
S.plot_uetc()
```
or
```
import string_realisations as SR
S = SR.Strings(C = SR.Constants(Omega_b = 0.25, Omega_c = 0.75, Omega_v = 0, Neff = 0.), S = SR.StringParameters(num_rel = 1000))
S.uetc(50, 1, 10**-2, 10**2, loop = 10)
S.plot_uetc()
```
