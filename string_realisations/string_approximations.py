import numpy as np

alphat = 1
v = 0.65
xi = 0.13

alphax = [np.pi/2, np.pi/5, np.pi/7, np.pi/9]
b11x = [0.1, 0.5, 0.05, 0.19]
b22x = [0.05, 0.02, 0.03, 0.21]
b13x = [0.13, 0.1, 0.2, 0.2]
b23x = [0.14, 0.08, 0.23, 0.23]

for i in xrange(4):
    alpha = alphax[i]
    b11 = b11x[i]
    b22 = b22x[i]
    b13 = b13x[i]
    b23 = b23x[i]

    print("alpha = ", alpha)
    print("b11 = ", b11)
    print("b22 = ", b22)
    print("b13 = ", b13)
    print("b23 = ", b23)

    s0s0s0 = -4 * np.pi**2 * (b11**2 * xi**2 + b13**2 * xi**2 - 2 * np.cos(alpha) * (b13 * b23 * xi**2 + 12 * (b13 - b11) * (b23 - b22) * v**2) + b22**2 * xi**2 + b23**2 * xi**2 + 12 * (b13 - b11)**2 * v**2 + 12 * (b23 - b22)**2 * v**2 - 18) / 9. 

    print("000 = ", s0s0s0)

    scalar = (-np.pi**2 * (384 * alphat**6 * (b13 - b11)**2 * v**8 - 576 * alphat**4 * (b13 - b11)**2 * v**8 + 288 * alphat**2 * (b13 - b11)**2 * v**8 - 48 * (b13 - b11)**2 * v**8 + 168 * alphat**6 * (b23 - b22)**2 * v**8 -252 * alphat**4 * (b23 - b22)**2 * v**8 - 36 * (b23 - b22)**2 * v**8 - 144 * alphat**6 * v**6 + 216 * alphat**4 * v**6 + 216 * alphat**2 * v**6 + 576 * alphat**4 * (b13 - b11)**2 * v**6 - 576 * alphat**2 * (b13 - b11)**2 * v**6 + 144 * (b13 - b11)**2 * v**6 + 252 * alphat**4 * (b23 - b22)**2 * v**6 + 72 * alphat**2 * (b23 - b22)**2 * v**6 - 180 * (b23 - b22)**2 * v**6 - 4 * alphat**6 * b11**2 * xi**2 * v**6 + 24 * alphat**4 * b11**2 * xi**2 * v**6 - 48 * alphat**2 * b11**2 * xi**2 * v**6 + 32 * b11**2 * xi**2 * v**6 - 4 * alphat**6 * b13**2 * xi**2 * v**6 + 24 * alphat**4 * b13**2 * xi**2 * v**6 - 48 * alphat**2 * b13**2 * xi**2 * v**6 + 32 * b13**2 * xi**2 * v**6 + 5 * alphat **6 * b22**2 * xi**2 * v**6 - 3 * alphat**4 * b22**2 * xi**2 * v**6 - 21 * alphat**2 * b22**2 * xi**2 * v**6 + 14 * b22**2 * xi**2 * v**6 + 5 * alphat**6 * b23**2 * xi**2 * v**6 - 3 * alphat**4 * b23**2 * xi**2 * v**6 - 21 * alphat**2 * b23**2 * xi**2 * v**6 + 14 * b23**2 * xi**2 * v**6 - 144 * v**6 - 216 * alphat*4 * v**4 - 432 * alphat**2 * v**4 + 288 * alphat**2 * (b13 - b11)**2 * v**4 - 144 * (b13 - b11)**2 * v**4 - 36 * alphat**2 * (b23 - b22)**2 * v**4 + 180 * (b23 - b22)**2 * v**4 - 24 * alphat**4 * b11**2 * xi**2 * v**4 + 96 * alphat**2 * b11**2 * xi**2 * v**4 - 96 * b11**2 * xi**2 * v**4 - 24 * alphat**4 * b13**2 * xi**2 * v**4 + 96 * alphat**2 * b13**2 * xi**2 * v**4 - 96 * b13**2 * xi**2 * v**4 + 3 * alphat**4 * b22**2 * xi**2 * v**4 + 42 * alphat**2 * b22**2 * xi**2 * v**4 - 42 * b22**2 * xi**2 * v**4 + 3 * alphat**4 * b23**2 * xi**2 * v**4 + 42 * alphat**2 * b23**2 * xi**2 * v**4 - 42 * b23**2 * xi**2 * v**4 + 432 * v**4 + 216 * alphat**2 * v**2 + 48 * (b13 - b11)**2 * v**2 - 60 * (b23 - b22)**2 * v**2 - 48 * alphat**2 * b11**2 * xi**2 * v**2 + 96 * b11**2 * xi**2 * v**2 - 48 * alphat**2 * b13**2 * xi**2 * v**2 + 96 * b13**2 * xi**2 * v**2 - 21 * alphat**2 * b22**2 * xi**2 * v**2 + 42 * b22**2 * xi**2 * v**2 - 21 * alphat**2 * b23**2 * xi**2 * v**2 + 42 * b23**2 * xi**2 * v**2 - 432 * v**2 - 32 * b11**2 * xi**2 - 32 * b13**2 * xi**2 - 14 * b22**2 * xi**2 - 14 * b23**2 * xi**2 - 8 * (12 * (b13 - b11) * (b23 - b22) * v**2 * ((2 * alphat**2 - 1) * v**2 + 1)**3 - b13 * b23 * ((alphat**2 - 2) * v**2 + 2)**3 * xi**2) * np.cos(alpha) + 9 * ((alphat**4 - alphat**2 + 1) * v**4 + (alphat**2 - 2) * v**2 + 1) * (12 * (b23 - b22)**2 * ((2 * alphat**2 - 1) * v**4 + v**2) - (b22**2 + b23**2) * ((alphat**2 - 2) * v**2 + 2) * xi**2) * np.cos(2 * alpha) + 144))/(315 * alphat**6)

    print("scalar = ", scalar)

    vector = (-4 * np.pi**2 * np.sin(alpha) * ((alphat**2 - 2) * v**2 + (alphat**4 - alphat**2 + 1) * v**4 + 1) * (-b13*b23 * xi**2 * ((alphat**2 - 2) * v**2 + 2) + np.cos(alpha) * (xi**2 * ((alphat**2 - 2) * v**2 + 2) * (b22**2 + b23**2) - 12 * (b23 - b22)**2 * ((2 * alphat**2 - 1) * v**4 + v**2)) + 12 * (b13 - b11) * (b23 - b22) * v**2 * ((2 * alphat**2 - 1) * v**2 + 1)))/(315 * alphat**6)

    print("vector = ", vector)

    tensor = 0

    print("tensor = ", tensor)
    print(" ")
    print(" ")
