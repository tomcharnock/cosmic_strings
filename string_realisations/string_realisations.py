import numpy as np
import tqdm
from scipy.integrate import romberg,odeint
from scipy.interpolate import interp1d, InterpolatedUnivariateSpline, RegularGridInterpolator

class Constants:
    def __init__(self, h0 = 70., Omega_k = 0., Omega_b = 0.0462, Omega_c = 0.2538, Omega_v = 0.7, Neff = 3.046):
        #! Define all constants for background evolution
    # h0      R Hubble value today in km/s/Mpc
    # Omega_k R Physical density due to curvature /h^2
    # Omega_b R Physical density due to cold dark matter /h^2
    # Omega_c R Physical density due to baryonic matter /h^2
    # Omega_v R Physical density due to dark energy /h^2
    # Neff    R Effective degrees of freedom in radiation
        self.G = 6.67428e-11         # R Newton's constant
        self.kappa = 8.*np.pi*self.G # R 8*pi*G
        self.s_B = 5.6704e-8         # R Boltzmann constant
        self.c = 2.99792458e8        # R Speed of light
        self.w = -1.                 # R Dark energy equation of state parameter
        self.T_CMB = 2.7255          # R Temperature of CMB
        self.Mpc = 3.085678e22       # R Units of mega-parsecs
        self.h0 = h0                 # R Hubble value today in km/s/Mpc
        self.Omega_k = Omega_k       # R Physical density due to curvature /h^2
        self.Omega_b = Omega_b       # R Physical density due to cold dark matter /h^2
        self.Omega_c = Omega_c       # R Physical density due to baryonic matter /h^2
        self.Omega_v = Omega_v       # R Physical density due to dark energy /h^2
        self.Neff = Neff             # R Effective degrees of freedom in radiation

        self.grhom = 3.*self.h0**2./self.c**2.*1000.**2.                                      # R Density matter normalisation
        self.grhor = 7./8.*(4./11.)**(4./3.)*self.Neff                                        # R Density radiation normalisation
        self.grhok = self.grhom*self.Omega_k                                                  # R Density due to curvature
        self.grhoc = self.grhom*self.Omega_c                                                  # R Density due to cold dark matter
        self.grhob = self.grhom*self.Omega_b                                                  # R Density due to baryonic matter
        self.grhov = self.grhom*self.Omega_v                                                  # R Density due to dark energy
        self.grhog = self.kappa/self.c**2.*4.*self.s_B/self.c**3.*self.T_CMB**4.*self.Mpc**2. # R Density due to photons
        self.grhornomass = self.grhor*self.grhog                                              # R Density due to massless neutrinos
        self.adotrad = np.sqrt((self.grhog+self.grhornomass)/3.)                              # R Time derivative of the scale factor in radiation
        
    def background(self,a):
    #! Background evolution in conformal units (returns dtau/da) 
    # a R Scale factor
        a2 = a**2                                                                                                 # R Square of the scale factor
        grhoa2 = self.grhok * a2 + (self.grhoc + self.grhob) * a + self.grhog + self.grhornomass + self.grhov * a**(1. - 3. * self.w) # R Friedmann Equation multiplied by a^2
        return np.sqrt(3. / grhoa2)                                                                                 

    def adtauda(self,a):
    #! Return a multiplied by dtau/da
    # a R Scale factor
        return self.background(a)*a

    def logspace(self, taui, tauf, num_step):
    #! Logarithmically spaced time grid (using this instead of numpy to handle second axis in array)
    # taui     R [num_rel] Initial time (must have a length of at least 1, i.e. taui = np.array([initial time]))
    # tauf     R [num_rel] Initial time (must have a length of at least 1, i.e. taui = np.array([initial time]))
    # num_step I Number of values in the grid
        dlnt = np.log10(tauf/taui)/(num_step-1.) # R [num_rel]           Grid step size
        tau = np.zeros([len(taui),num_step])     # R [num_rel, num_step] Logarithmically spaced time grid
        for i in range(num_step):
            tau[:,i] = taui*10.**(i*dlnt)
        return tau

class StringParameters:
    def __init__(self, gmu = 1.1e-6, alpha = 1., cr = 0.23, v = 0.65, xi = 0.13, num_string = 200, num_rel = 1000, s = 1):
        #! Define all important string parameters
    # gmu        R String tension
    # alpha      R Wiggliness parameter
    # cr         R Loop chopping efficiency
    # v          R Initial velocity
    # xi         R Initial correlation length (comoving)
    # num_string I Number of consolidated string segments
    # num_rel    I Number of string realisations
    # s          I Seed to intialise random state 
        self.gmu = gmu                # R String tension
        self.alpha = alpha            # R Wiggliness parameter
        self.cr = cr                  # R Loop chopping efficiency
        self.v = v                    # R Initial velocity
        self.xi = xi                  # R Initial correlation length (comoving)
        self.num_string = num_string  # I Number of consolidated string segments
        self.num_rel = num_rel        # I Number of string realisations
        self.s = s                    # I Initial seed
        np.random.seed(seed = self.s) #! Initialise random state 

class Parameters:
    def __init__(self, num_step = 1000, tau_init = 2e-3, a_end = 5.):
    #! Define optional parameters for other classes
    # num_step I                      Number of evolution time steps
    # tau_init R                      Initial evolution time
        self.num_step = num_step
        self.tau_init = tau_init
        self.a_end = a_end

class Evolution:
    def __init__(self, C = Constants(), S = StringParameters(), P = Parameters()):
    #! Run string network evolution using the velocity one-scale model
    # num_step I                      Number of evolution time steps
    # tau_init R                      Initial evolution time
        self.num_step = P.num_step                                                                        # I                      Number of time steps
        self.tau_init = P.tau_init                                                                        # R                      Initial evolution time
        self.C = C                                                                                        # Constants Class
        self.S = S                                                                                        # StringParameters Class 
        self.tau_f = romberg(self.C.background, 0., P.a_end)                                              # R                      Time until scale factor (a) = a_end
        self.tau = self.C.logspace(np.array([self.tau_init]), np.array([self.tau_f]), self.num_step)[0]   # R [num_step]           Logarithmically spaced time grid
        self.xi, self.v = self.evolution()                                                                #! Calculate correlation lengths and velocities spline objects

    def evolution(self):
    #! Evolution of the correlation lengths and velocities of the string network
        t0 = 0.1*self.tau[0]                                  # R              Time to solve the differential equation from
        init_cond = [self.C.adotrad*t0, self.S.xi, self.S.v]  # R [3]          Initial condition for the string
        temp = odeint(self.evolve, init_cond, self.tau)       # R [num_step,3] Array containing evolution of a, xi and v

        interp_time = np.insert(np.insert(self.tau, 0, t0), -1, 1.1*self.tau[-1])    # R [num_step+2] Extended time grid to extrapolate outside of time window
        interp_xi = np.insert(np.insert(temp[:, 1], 0, temp[0, 1]), -1, temp[-1, 1]) # R [num_step+2] Extended correlation lengths to extrapolate outside of time window
        interp_v = np.insert(np.insert(temp[:, 2], 0, temp[0, 2]), -1, temp[-1, 2])  # R [num_step+2] Extended velocities to extrapolate outside of time window

        #xi = InterpolatedUnivariateSpline(self.tau,temp[:, 1]) # Obj            Interpolated spline of the correlation length at a given comoving time
        #v = InterpolatedUnivariateSpline(self.tau,temp[:, 2])  # Obj            Interpolated spline of the velocity at a given comoving time

        xi = InterpolatedUnivariateSpline(interp_time, interp_xi) # Obj            Interpolated spline of the correlation length at a given comoving time
        v = InterpolatedUnivariateSpline(interp_time, interp_v)   # Obj            Interpolated spline of the velocity at a given comoving time
        return xi, v

    def evolve(self,x,tau): 
    #! Velocity one-scale model evolution, for use with differential equation integrator
    # x   R [3] Array containing scale factor, correlation length and velocity to be evolved
    # tau R     Evolution time
        adotoa = 1./(self.C.background(x[0])*x[0]) 	                                                            # R Hubble function
        adot = adotoa*x[0]                                                                                      # R Time derivative of the scale factor
        xidot = (2.*tau*adotoa*x[1]*x[2]**2.+self.S.cr*x[2]-2.*x[1])/(2.*tau)                                       # R Time derivative of the correlation length 
        vdot = (x[2]**2.-1.)*(2.*tau*adotoa*x[2]-(2.*np.sqrt(2.)/np.pi)*(1.-8.*x[2]**6.)/(1.+8.*x[2]**6.)/x[1])/tau # R Time derivative of the velocity
        return adot, xidot, vdot
    
    def evolution_plot(self):
    #! Log-lin plot of the correlation length in upper subplot and velocity in lower subplot
    #! Must run E=Evolution() before calling E.evolution_plot()
        import matplotlib.pyplot as plt
        import matplotlib.gridspec as g
        params = {'text.usetex': False, 'mathtext.fontset': 'stixsans', 'figure.figsize': [3, 2.25], 'legend.fontsize': 8, 'font.size': 8}
        plt.rcParams.update(params)

        fig = plt.figure(figsize=(3.5, 3))
        gs1 = g.GridSpec(100, 100)
        gs1.update(left=0.12, right=0.95, top=0.95, bottom=0.12, wspace=0, hspace=0)
        ax1 = plt.subplot(gs1[0:50, :])
        ax2 = plt.subplot(gs1[50:, :])

        xi, = ax1.plot(self.tau, self.xi(self.tau))
        v, = ax2.plot(self.tau, self.v(self.tau))

        ax1.set_xscale('log')
        ax1.set_xlim([self.tau_init, self.tau_f])
        ax1.set_xticklabels([])
        ax1.set_yticks([0.0, 0.1, 0.2, 0.3, 0.4])
        ax1.set_ylim([-0.05, 0.45])
        ax1.set_ylabel('$\\xi$', labelpad=0)

        ax2.set_xscale('log')
        ax2.set_xlim([self.tau_init, self.tau_f])
        ax2.set_yticks([0.5, 0.6, 0.7])
        ax2.set_ylim([0.45, 0.73])
        ax2.set_xlabel('$\\tau$', labelpad=0)
        ax2.set_ylabel('$v$', labelpad=0)
        
        plt.savefig('plots/VOS.pdf')
        plt.show()

class Initialise:
    def __init__(self, C = Constants(), S = StringParameters(), P = Parameters(), E = None):
    #! Calculate scaling normalisation C(tau) and initialise random orientations and velocities for each consolidated string and each string realisation
    # C Constants Class        Initialise the Constants Class
    # S StringParameters Class Initialise the StringParameters Class
        self.C = C                                                               # Constants Class             Initialise the Constants Class
        self.S = S                                                               # StringParameters Class      Initialise the StringParameters Class
        self.P = P
        if E is None:
            self.E = Evolution(C = self.C, S = self.S, P = self.P) 
        else:
            self.E = E                                                       # Evolution Class             Initialise the Evolution Class
        np.random.seed(seed = S.s) #! Initialise random state 
        self.tau = self.timegrid()                                           # R [num_rel, num_string+1]   Logarithmically spaced time grid
        self.Ct = self.scaling()                                             # Obj                         Interpolated spline of the normalisation for scaling, C(tau)
        self.soz, self.voz, self.sp, self.vp, self.tp = self.string_properties() # R [num_rel, num_strings] x2 z-axis string and velocity orientations
                                             # Obj [num_strings] x3        Interpolated splines of the scalar, vector, tensor prefactors

    def timegrid(self):
    #! Create time grid from soon after the initial evolution time to soon after the final evolution time for strings switching off
        taui = self.E.tau_init*(1.+np.random.rand(self.S.num_rel)) # R [num_rel]               Random initial time after initial evolution time
        tauf = self.E.tau[-1]*(1.+np.random.rand(self.S.num_rel))  # R [num_rel]               Random final time after final evolution time
        tau = self.C.logspace(taui, tauf, self.S.num_string)       # R [num_rel, num_string]   Logarithmically spaced time grid
        tau = np.insert(tau, 0, self.E.tau_init, axis=1)           # R [num_rel, num_string+1] Add initial evolution time as first element
        return tau

    def scaling(self):
    #! Calculate normalisation for the scaling parameter such that strings scale as 1/(xi*tau)**3
        xitau = self.E.xi(self.tau)*self.tau                 # R [num_rel, num_string+1] xi*tau evaluated at each time
        C = np.zeros([self.S.num_rel, self.E.num_step])      # R [num_rel, num_step]     Normalisation constant for string scaling
        Nd = np.zeros([self.S.num_rel, self.S.num_string+1]) # R [num_rel, num_string+1] Difference of 1/(xi*tau)**3 at each time step
        Nd[:, -1] =  1./xitau[:, -1]**3.
        for i in range(1,self.S.num_string):
            Nd[:, i] = 1./xitau[:, i-1]**3. - 1./xitau[:, i]**3.

        for i in range(self.E.num_step):
            C[:, i] = 1./(np.sum((self.E.tau[i] <= self.tau).astype('float')*Nd, axis=1)*(self.E.tau[i]*self.E.xi(self.E.tau[i]))**3.)

        return interp1d(self.E.tau,C)

    def string_properties(self):
    #! Initialise random orientations and velocities of each consolidated string and each string realisation
    #! Calculates the prefactors for the scalar, vector and tensor energy-momentum tensors
        self.phases = 2.*np.pi*np.random.rand(self.S.num_rel*self.S.num_string).reshape([self.S.num_rel,self.S.num_string]) # R [num_rel, num_string] Initial phases
        theta = np.pi*np.random.rand(self.S.num_rel*self.S.num_string).reshape([self.S.num_rel,self.S.num_string])          # R [num_rel, num_string] Initial theta
        phi = 2.*np.pi*np.random.rand(self.S.num_rel*self.S.num_string).reshape([self.S.num_rel,self.S.num_string])         # R [num_rel, num_string] Initial phi
        psi = 2.*np.pi*np.random.rand(self.S.num_rel*self.S.num_string).reshape([self.S.num_rel,self.S.num_string])         # R [num_rel, num_string] Initial psi                             
        costheta = np.cos(theta) # R [num_rel, num_string] Initial cos(theta)
        sintheta = np.sin(theta) # R [num_rel, num_string] Initial sin(theta)
        cosphi = np.cos(phi)     # R [num_rel, num_string] Initial cos(phi)
        sinphi = np.sin(phi)     # R [num_rel, num_string] Initial sin(phi)
        cospsi = np.cos(psi)     # R [num_rel, num_string] Initial cos(psi)
        sinpsi = np.sin(psi)     # R [num_rel, num_string] Initial sin(psi)
        del theta
        del phi
        del psi

        so = np.array([sintheta*cosphi, sintheta*sinphi, costheta])                                                   # R [3, num_rel, num_string] String orientations
        vo = np.array([costheta*cosphi*cospsi-sinphi*sinpsi, costheta*sinphi*cospsi+cosphi*sinpsi, -sintheta*cospsi]) # R [3, num_rel, num_string] Velocity orientations
        del costheta
        del sintheta
        del cosphi
        del sinphi
        del cospsi
        del sinpsi
        
        v = self.E.v(self.E.tau)   # R [num_step] Velocities at each time step
        xi = self.E.xi(self.E.tau) # R [num_step] Correlation lengths at each time step

        sp = 0.5*(v[:, None, None]**2.*(2.*vo[2, :, :]**2.-vo[0, :, :]**2.-vo[1, :, :]**2.)                                  # R [num_step, num_rel, num_string] Scalar prefactor
                        -(1.-v[:, None, None]**2.)/self.S.alpha**2.*(2.*so[2, :, :]**2.-so[0, :, :]**2.-so[1, :, :]**2.))
        vp = v[:, None, None]**2.*vo[0, :, :]*vo[2, :, :]-(1.-v[:, None, None]**2.)/self.S.alpha**2.*so[0, :, :]*so[2, :, :] # R [num_step, num_rel, num_string] Vector prefactor
        tp = v[:, None, None]**2.*vo[0, :, :]*vo[1, :, :]-(1.-v[:, None, None]**2.)/self.S.alpha**2.*so[0, :, :]*so[1, :, :] # R [num_step, num_rel, num_string] Tensor prefactor
        so = so[2]
        vo = vo[2]

        sp_spline = [] # Obj [num_string] Spline interpolation object of the scalar prefactor for each consolidated string 
        vp_spline = [] # Obj [num_string] Spline interpolation object of the vector prefactor for each consolidated string 
        tp_spline = [] # Obj [num_string] Spline interpolation object of the vector prefactor for each consolidated string 
        for i in range(self.S.num_string):
            sp_spline.append(interp1d(self.E.tau,np.swapaxes(sp, 0, 1)[:,:,i]))
            vp_spline.append(interp1d(self.E.tau,np.swapaxes(vp, 0, 1)[:,:,i]))
            tp_spline.append(interp1d(self.E.tau,np.swapaxes(tp, 0, 1)[:,:,i]))

        return so, vo, sp_spline, vp_spline, tp_spline

class Strings:
    def __init__(self, C = Constants(), S = StringParameters(), P = Parameters(), E = None):
    #! Calculates the evolution of the string network and initialises the string properties for use in calculating the energy momentum tensors
    # C Constants Class          Initialise the Constants Class
    # S StringParameters Class   Initialise the StringParameters Class
        self.C = C # Constants Class          Initialise the Constants Class
        self.S = S # StringParameters Class   Initialise the StringParameters Class
        self.P = P
        if E is None:
            self.E = Evolution(C = self.C, S = self.S, P = self.P) 
        else:
            self.E = E
        self.I = Initialise(C = self.C, S = self.S, P = self.P, E = self.E)         # Initialise Class	     Initialise the Initialise Class 

    def emt(self, k, tau):
    #! Works out the energy momentum tensor of the 00, scalar, vector and tensor components of strings
    # k   R        Value of the wavenumber to calculate the energy momentum tensor at
    # tau R [grid] Times at which to evaluated the energy momentum tensor with length=grid
        grid = len(tau) # I The number of times to evaluate the energy momentum tensor at

        v = self.E.v(tau)   # R [grid] Velocities at each time step
        xi = self.E.xi(tau) # R [grid] Correlation lengths at each time step

        Ct = np.swapaxes(self.I.Ct(tau), 0, 1)                     # R [grid, num_rel]             Scaling normalisation at each tau
        xitau = self.I.tau*self.E.xi(self.I.tau)                 # R [num_rel, num_string+1]     xi*tau at each 
        Nd = np.zeros([self.S.num_rel, self.S.num_string])       # R [num_rel, num_string]       Unnormalised number of strings which decay between each time step
        sp = np.zeros([grid, self.S.num_rel, self.S.num_string]) # R [grid, num_rel, num_string] Scalar emt prefactor
        vp = np.zeros([grid, self.S.num_rel, self.S.num_string]) # R [grid, num_rel, num_string] Vector emt prefactor
        tp = np.zeros([grid, self.S.num_rel, self.S.num_string]) # R [grid, num_rel, num_string] Tensor emt prefactor
        Nd[:, -1] = 1./xitau[:, -1]**3.
        for i in range(self.S.num_string):
            if i > 0:
                Nd[:, i-1] = 1./xitau[:, i-1]**3. - 1./xitau[:, i]**3.
            sp[:, :, i] = np.swapaxes(self.I.sp[i](tau), 0, 1)
            vp[:, :, i] = np.swapaxes(self.I.vp[i](tau), 0, 1)
            tp[:, :, i] = np.swapaxes(self.I.tp[i](tau), 0, 1)
        Nd[Nd<0] = 0. #! The number of strings can become negative if there is non-zero cosmological constant which makes the realisations fail.
                      #! This prevents the code from failing and will set any unknown region to zero.

        EMT00 = np.zeros([self.S.num_rel, grid]) # R [num_rel, grid] 00 component of the energy momentum tensor for each string realisation at each time step
        EMTS = np.zeros([self.S.num_rel, grid])  # R [num_rel, grid] Scalar component of the energy momentum tensor for each string realisation at each time step
        EMTV = np.zeros([self.S.num_rel, grid])  # R [num_rel, grid] Vector component of the energy momentum tensor for each string realisation at each time step
        EMTT = np.zeros([self.S.num_rel, grid])  # R [num_rel, grid] Tensor component of the energy momentum tensor for each string realisation at each time step
        for i in range(grid):
            scaling = np.sqrt(Ct[i, :, None]*(tau[i] < self.I.tau).astype('float')[:, :-1]*Nd) # R [num_rel, num_string] Scaling factor of the strings
                                                      #! If the grid time is greater than the time that consolidated string
                                                      #! switches off it this is zero, i.e. Lf is not implimented at all. 

            t00 = np.sqrt(2)*(scaling*self.S.alpha/np.sqrt(1.-v[i]**2.) # R [num_rel, num_string] Energy momentum tensor of each consolidated string
                                     * np.sin(k*self.I.soz*xi[i]*tau[i]/2.)/(k*self.I.soz/2.) #! Technically this is sqrt(k*tau) times the energy momentum tensor
                                     * np.cos(self.I.phases+k*self.I.voz*v[i]*tau[i]))*np.sqrt(k*tau[i])
            EMT00[:, i] = np.sum(t00, axis = 1)
            EMTS[:, i] = np.sum(t00*sp[i, :, :], axis = 1)
            EMTV[:, i] = np.sum(t00*vp[i, :, :], axis = 1)
            EMTT[:, i] = np.sum(t00*tp[i, :, :], axis = 1)
        return [EMT00, EMTS, EMTV, EMTT]
        
    def uetc(self, grid, k, taui, tauf, loop = 0):
    #! Calculates the unequal time correlator on a grid
    #! Must run S=Strings() before calling S.uetc(grid, k, taui, tauf)
    # grid I Number of time steps to evaluate the uetc at
    # k    R Wave number to be evaluated
        # taui R Intial time to evaluate the uetc
    # tauf R Final time to evaluate the uetc
    # loop I Number of times to loop over the uetc calculation 
        #!       (can be faster, i.e. loop = 10, num_rel = 1000 is faster than loop = 1, num_rel = 10000,
        #!        but loop = 1, num_rel = 1000 is much faster than loop = 1000, num_rel = 1)
            
        self.I = Initialise(C = self.C, S = self.S, E = self.E) # Initialise Class	     Initialise the Initialise Class 
        ktau = k*self.C.logspace(np.array([taui]), np.array([tauf]), grid)[0] # R [grid] Logarithmically spaced time grid

        if loop == 0:
            EMT00, EMTS, EMTV, EMTT = self.emt(1, ktau) #! Calculates the energy momentum tensor of each component at every point on the time grid

            SS00 = np.tensordot(EMT00, EMT00, axes=([0], [0]))/self.S.num_rel  # R [grid, grid] 00 component of the unequal time correlator
            SS = np.tensordot(EMTS, EMTS, axes=([0], [0]))/self.S.num_rel      # R [grid, grid] Scalar component of the unequal time correlator
            VV = np.tensordot(EMTV, EMTV, axes=([0], [0]))/self.S.num_rel      # R [grid, grid] Vector component of the unequal time correlator
            TT = np.tensordot(EMTT, EMTT, axes=([0], [0]))/self.S.num_rel      # R [grid, grid] Tensor component of the nequal time correlator
            SS00cross = 0.5*(np.tensordot(EMT00, EMTS, axes=([0], [0]))        # R [grid, grid] 00-Scalar component of the unequal time correlator
                     +np.tensordot(EMTS, EMT00, axes=([0], [0])))/self.S.num_rel
        else:
            SS00 = np.zeros([grid, grid])      # R [grid, grid] 00 component of the unequal time correlator
            SS = np.zeros([grid, grid])        # R [grid, grid] Scalar component of the unequal time correlator
            VV = np.zeros([grid, grid])        # R [grid, grid] Vector component of the unequal time correlator
            TT = np.zeros([grid, grid])        # R [grid, grid] Tensor component of the unequal time correlator
            SS00cross = np.zeros([grid, grid]) # R [grid, grid] 00-Scalar cross-component of the unequal time correlator

            for l in tqdm.tqdm(range(loop)):
                EMT00, EMTS, EMTV, EMTT = self.emt(1, ktau) #! Calculates the energy momentum tensor of each component at every point on the time grid

                SS00 += np.tensordot(EMT00, EMT00, axes=([0], [0]))/self.S.num_rel           
                SS += np.tensordot(EMTS, EMTS, axes=([0], [0]))/self.S.num_rel
                VV += np.tensordot(EMTV, EMTV, axes=([0], [0]))/self.S.num_rel
                TT += np.tensordot(EMTT, EMTT, axes=([0], [0]))/self.S.num_rel
                SS00cross += 0.5*(np.tensordot(EMT00, EMTS, axes=([0], [0]))
                     +np.tensordot(EMTS, EMT00, axes=([0], [0])))/self.S.num_rel
                self.S.s = l+1
                self.I = Initialise(C = self.C, S = self.S)

            SS00 = SS00/loop
            SS = SS/loop
            VV = VV/loop
            TT = TT/loop
            SS00cross = SS00cross/loop
        SS00_spline = RegularGridInterpolator((ktau, ktau), SS00)
        SS_spline = RegularGridInterpolator((ktau, ktau), SS)
        VV_spline = RegularGridInterpolator((ktau, ktau), VV)
        TT_spline = RegularGridInterpolator((ktau, ktau), TT)
        SS00cross_spline = RegularGridInterpolator((ktau, ktau), SS00cross)
        return SS00_spline, SS_spline, VV_spline, TT_spline, SS00cross_spline

    def calculate_correlator(self, ktau):
        EMT00, EMTS, EMTV, EMTT = self.emt(1, ktau) #! Calculates the energy momentum tensor of each component at every point on the time grid
        SSS000 = np.einsum('ij,ikl->jkl', EMT00, np.einsum('ij,ik->ijk', EMT00, EMT00))/self.S.num_rel
        SSS = np.einsum('ij,ikl->jkl', EMTS, np.einsum('ij,ik->ijk', EMTS, EMTS))/self.S.num_rel
        VVV = np.einsum('ij,ikl->jkl', EMTV, np.einsum('ij,ik->ijk', EMTV, EMTV))/self.S.num_rel
        TTT = np.einsum('ij,ikl->jkl', EMTT, np.einsum('ij,ik->ijk', EMTT, EMTT))/self.S.num_rel
        return SSS000, SSS, VVV, TTT
    
    def calculate_correlator_single(self, ktau1, ktau2, ktau3, loop = 1):
        EMT001, EMTS1, EMTV1, EMTT1 = self.emt(1, ktau1) #! Calculates the energy momentum tensor of each component at every point on the time grid
        EMT002, EMTS2, EMTV2, EMTT2 = self.emt(1, ktau2) #! Calculates the energy momentum tensor of each component at every point on the time grid
        EMT003, EMTS3, EMTV3, EMTT3 = self.emt(1, ktau3) #! Calculates the energy momentum tensor of each component at every point on the time grid
        if loop == 1:
            EMT001, EMTS1, EMTV1, EMTT1 = self.emt(1, ktau1) #! Calculates the energy momentum tensor of each component at every point on the time grid
            EMT002, EMTS2, EMTV2, EMTT2 = self.emt(1, ktau2) #! Calculates the energy momentum tensor of each component at every point on the time grid
            EMT003, EMTS3, EMTV3, EMTT3 = self.emt(1, ktau3) #! Calculates the energy momentum tensor of each component at every point on the time grid
            SSS000 = np.mean(EMT001[:, 0] * EMT002[:, 0] * EMT003[:, 0])
            SSS = np.mean(EMTS1[:, 0] * EMTS2[:, 0] * EMTS3[:, 0])
            VVV = np.mean(EMTV1[:, 0] * EMTV2[:, 0] * EMTV3[:, 0])
            TTT = np.mean(EMTT1[:, 0] * EMTT2[:, 0] * EMTT3[:, 0])
        else:
            SSS000 = 0
            SSS = 0
            VVV = 0
            TTT = 0
            for l in tqdm.tqdm(range(loop)):
                EMT001, EMTS1, EMTV1, EMTT1 = self.emt(1, ktau1) #! Calculates the energy momentum tensor of each component at every point on the time grid
                EMT002, EMTS2, EMTV2, EMTT2 = self.emt(1, ktau2) #! Calculates the energy momentum tensor of each component at every point on the time grid
                EMT003, EMTS3, EMTV3, EMTT3 = self.emt(1, ktau3) #! Calculates the energy momentum tensor of each component at every point on the time grid
                SSS000 += np.mean(EMT001[:, 0] * EMT002[:, 0] * EMT003[:, 0])
                SSS += np.mean(EMTS1[:, 0] * EMTS2[:, 0] * EMTS3[:, 0])
                VVV += np.mean(EMTV1[:, 0] * EMTV2[:, 0] * EMTV3[:, 0])
                TTT += np.mean(EMTT1[:, 0] * EMTT2[:, 0] * EMTT3[:, 0])
                self.S.s = l+1
                self.I = Initialise(C = self.C, S = self.S)

            SSS000 = SSS000/loop
            SSS = SSS/loop
            VVV = VVV/loop
            TTT = TTT/loop
        return SSS000, SSS, VVV, TTT
        
    def get_ktau(self, grid, k, taui, tauf):
        if grid > 1:
            return self.C.logspace(np.array([np.min(k)*taui]), np.array([np.max(k)*tauf]), grid)[0] # R [grid] Logarithmically spaced k*tau grid
        else:
            return np.array([np.min(k) * taui])  # R [grid] Logarithmically spaced k*tau grid

    def correlator_interpolator(self, ktau, correlators):
        SSS000_spline = RegularGridInterpolator((ktau, ktau, ktau), correlators[0])
        SSS_spline = RegularGridInterpolator((ktau, ktau, ktau), correlators[1])
        VVV_spline = RegularGridInterpolator((ktau, ktau, ktau), correlators[2])
        TTT_spline = RegularGridInterpolator((ktau, ktau, ktau), correlators[3])
        return SSS000_spline, SSS_spline, VVV_spline, TTT_spline

    def plot_uetc(self, data, grid, k, taui, tauf):
    #! Creates a set of contour plots for the 00, Scalar, Vector, Tensor and 00-Scalar components of the string unequal time correlators 
    #! Must run S=Strings() before calling S.plot_uetc()
        import matplotlib.pyplot as plt
        import matplotlib.gridspec as g
        params = {'text.usetex': False, 'mathtext.fontset': 'stixsans', 'legend.fontsize': 8, 'font.size': 8}
        plt.rcParams.update(params)

        fig = plt.figure(figsize = (8, 4.5))
        gs = g.GridSpec(2, 3)
        gs.update(left = 0.12, right = 0.95, top = 0.95, bottom = 0.12, wspace = 0.5, hspace = 0.3)
        
        ktau = self.get_ktau(grid, k, taui, tauf)
        
        j = 0
        k = 0
        for i in range(5):
            plot = np.zeros([grid, grid])
            for l in range(grid):
                plot[l, :] = data[i]([[ktau[l], ktau[m]] for m in range(grid)])

            ax = plt.subplot(gs[k, j])
            c = ax.contourf(np.log10(ktau), np.log10(ktau), plot)
            plt.colorbar(c, ax = ax, pad = 0.)
            plt.xticks([min(np.log10(ktau)), (abs(max(np.log10(ktau)))-abs(min(np.log10(ktau))))/2., max(np.log10(ktau))])
            ax.set_xlabel('$\log k\\tau$', labelpad=0.)
            ax.set_ylabel('$\log k\\tau$', labelpad=0.)
            j += 1
            if j == 3:
                k += 1
                j = 0
        #plt.savefig('plots/uetc.pdf')
        plt.show()

    def plot_threepointcorrelator(self, data, grid, k, taui, tauf, correlator, folder):
    #! Creates a set of contour plots for different combinations of the 00, Scalar, Vector, Tensor and 00-Scalar components of the bispectrum
    #! Must run S=Strings() before calling S.plot_bispectrum()
        import matplotlib.pyplot as plt
        import matplotlib.gridspec as g

        params = {'text.usetex': False, 'mathtext.fontset': 'stixsans', 'legend.fontsize': 8, 'font.size': 8}
        plt.rcParams.update(params)

        fig = plt.figure(figsize=(8, 8))
        gs = g.GridSpec(3, 3)
        gs.update(left=0.12, right=0.95, top=0.95, bottom=0.12, wspace=0.3, hspace=0.3)

        i_label = {0: '$\log\\beta_{11}$', 1: '$\log\\beta_{22}$', 2: '$\log\\beta_{13}$'}
        j_label = {0: '$\log\\beta_{22}$', 1: '$\log\\beta_{13}$', 2: '$\log\\beta_{23}$'}

        if correlator == '000':
            cor = 0
        if correlator == 'SSS':
            cor = 1
        if correlator == 'VVV':
            cor = 2
        if correlator == 'TTT':
            cor = 3

        tau = self.C.logspace(np.array([taui]),np.array([tauf]),grid)[0] 

        # (b13 == b23) < b11, b22
        b11 = k[0]*tau
        b22 = k[1]*tau
        b13 = k[0]*tau
        b23 = k[1]*tau

        i_plot = [b11, b22, b13]
        j_plot = [b22, b13, b23]

        plot = np.zeros([grid, 6, grid, grid])
        for i in range(grid):
            for j in range(grid):
                for l in range(grid):
                    plot[l, 0, i, j] = data[cor]([b11[i], b22[j], b13[l]])

                    if (j >= l-1) and (j <= l+1):
                        plot[l, 1, i, j] = data[cor]([b11[i], b13[j], b13[l]])
                        plot[l, 2, i, j] = data[cor]([b11[i], b13[j], b13[l]])
                        plot[l, 3, i, j] = data[cor]([b22[i], b13[j], b13[l]])
                        plot[l, 4, i, j] = data[cor]([b22[i], b13[j], b13[l]])
                        if (i >= l-1) and (i <= l+1):
                            plot[l, 5, i, j] = data[cor]([b13[i], b13[j], b13[l]])
                        else:
                            plot[l, 5, i, j] = np.nan
                    else:
                        plot[l, 1, i, j] = np.nan
                        plot[l, 2, i, j] = np.nan
                        plot[l, 3, i, j] = np.nan
                        plot[l, 4, i, j] = np.nan
                        plot[l, 5, i, j] = np.nan

        mini = np.min(np.ma.masked_invalid(plot))
        maxi = np.max(np.ma.masked_invalid(plot))
        for l in tqdm.tqdm(range(grid)):
            fig = plt.figure(figsize=(8, 6))
            gs = g.GridSpec(3, 3)
            gs.update(left=0.07, right=0.95, top=0.95, bottom=0.12, wspace=0.4, hspace=0.3)
            plot_num = 0
            for i in range(3):
                for j in range(i, 3):
                    ax = plt.subplot(gs[i,j])
                    ax.set_xlabel(j_label[j], labelpad = 0)
                    ax.set_ylabel(i_label[i], labelpad = 0)
                    c = ax.contourf(np.log10(i_plot[i]), np.log10(j_plot[j]), plot[l, plot_num, :, :], levels = np.linspace(mini, maxi, 25))
                    plt.colorbar(c, ax = ax, pad = 0.)
                    plt.xticks([min(np.log10(j_plot[j])), (abs(max(np.log10(j_plot[j])))-abs(min(np.log10(j_plot[j]))))/2., max(np.log10(j_plot[j]))])
                    plot_num += 1

            plt.savefig('plots/'+folder+'/3ptcorrelator_'+str(l)+'_'+correlator+'.pdf')
            plt.close()      

    def save(self, filename, data):
        np.save(filename, data)

    def load(self, filename):
        return np.load(filename)

class Approximations:
    import scipy.special as ss
    
    def __init__(self, k, tau, a, xi = 0.13, v = 0.65, alpha = 1.):
        self.b11 = k[0] * tau[0] / 2.
        self.b13 = k[0] * tau[2] / 2.
        self.b22 = k[1] * tau[1] / 2.
        self.b23 = k[1] * tau[2] / 2.
        self.a = a
        self.xi = xi
        self.v = v
        self.alpha = alpha
        
    def small_beta(self):
        SSS000 = -4 * np.pi**2 * (b11**2 * xi**2 + b13**2 * xi**2 - 2 * np.cos(alpha) * (b13 * b23 * xi**2 + 12 * (b13 - b11) * (b23 - b22) * v**2) + b22**2 * xi**2 + b23**2 * xi**2 + 12 * (b13 - b11)**2 * v**2 + 12 * (b23 - b22)**2 * v**2 - 18) / 9. 

        SSS = (-np.pi**2 * (384 * alphat**6 * (b13 - b11)**2 * v**8 - 576 * alphat**4 * (b13 - b11)**2 * v**8 + 288 * alphat**2 * (b13 - b11)**2 * v**8 - 48 * (b13 - b11)**2 * v**8 + 168 * alphat**6 * (b23 - b22)**2 * v**8 -252 * alphat**4 * (b23 - b22)**2 * v**8 - 36 * (b23 - b22)**2 * v**8 - 144 * alphat**6 * v**6 + 216 * alphat**4 * v**6 + 216 * alphat**2 * v**6 + 576 * alphat**4 * (b13 - b11)**2 * v**6 - 576 * alphat**2 * (b13 - b11)**2 * v**6 + 144 * (b13 - b11)**2 * v**6 + 252 * alphat**4 * (b23 - b22)**2 * v**6 + 72 * alphat**2 * (b23 - b22)**2 * v**6 - 180 * (b23 - b22)**2 * v**6 - 4 * alphat**6 * b11**2 * xi**2 * v**6 + 24 * alphat**4 * b11**2 * xi**2 * v**6 - 48 * alphat**2 * b11**2 * xi**2 * v**6 + 32 * b11**2 * xi**2 * v**6 - 4 * alphat**6 * b13**2 * xi**2 * v**6 + 24 * alphat**4 * b13**2 * xi**2 * v**6 - 48 * alphat**2 * b13**2 * xi**2 * v**6 + 32 * b13**2 * xi**2 * v**6 + 5 * alphat **6 * b22**2 * xi**2 * v**6 - 3 * alphat**4 * b22**2 * xi**2 * v**6 - 21 * alphat**2 * b22**2 * xi**2 * v**6 + 14 * b22**2 * xi**2 * v**6 + 5 * alphat**6 * b23**2 * xi**2 * v**6 - 3 * alphat**4 * b23**2 * xi**2 * v**6 - 21 * alphat**2 * b23**2 * xi**2 * v**6 + 14 * b23**2 * xi**2 * v**6 - 144 * v**6 - 216 * alphat*4 * v**4 - 432 * alphat**2 * v**4 + 288 * alphat**2 * (b13 - b11)**2 * v**4 - 144 * (b13 - b11)**2 * v**4 - 36 * alphat**2 * (b23 - b22)**2 * v**4 + 180 * (b23 - b22)**2 * v**4 - 24 * alphat**4 * b11**2 * xi**2 * v**4 + 96 * alphat**2 * b11**2 * xi**2 * v**4 - 96 * b11**2 * xi**2 * v**4 - 24 * alphat**4 * b13**2 * xi**2 * v**4 + 96 * alphat**2 * b13**2 * xi**2 * v**4 - 96 * b13**2 * xi**2 * v**4 + 3 * alphat**4 * b22**2 * xi**2 * v**4 + 42 * alphat**2 * b22**2 * xi**2 * v**4 - 42 * b22**2 * xi**2 * v**4 + 3 * alphat**4 * b23**2 * xi**2 * v**4 + 42 * alphat**2 * b23**2 * xi**2 * v**4 - 42 * b23**2 * xi**2 * v**4 + 432 * v**4 + 216 * alphat**2 * v**2 + 48 * (b13 - b11)**2 * v**2 - 60 * (b23 - b22)**2 * v**2 - 48 * alphat**2 * b11**2 * xi**2 * v**2 + 96 * b11**2 * xi**2 * v**2 - 48 * alphat**2 * b13**2 * xi**2 * v**2 + 96 * b13**2 * xi**2 * v**2 - 21 * alphat**2 * b22**2 * xi**2 * v**2 + 42 * b22**2 * xi**2 * v**2 - 21 * alphat**2 * b23**2 * xi**2 * v**2 + 42 * b23**2 * xi**2 * v**2 - 432 * v**2 - 32 * b11**2 * xi**2 - 32 * b13**2 * xi**2 - 14 * b22**2 * xi**2 - 14 * b23**2 * xi**2 - 8 * (12 * (b13 - b11) * (b23 - b22) * v**2 * ((2 * alphat**2 - 1) * v**2 + 1)**3 - b13 * b23 * ((alphat**2 - 2) * v**2 + 2)**3 * xi**2) * np.cos(alpha) + 9 * ((alphat**4 - alphat**2 + 1) * v**4 + (alphat**2 - 2) * v**2 + 1) * (12 * (b23 - b22)**2 * ((2 * alphat**2 - 1) * v**4 + v**2) - (b22**2 + b23**2) * ((alphat**2 - 2) * v**2 + 2) * xi**2) * np.cos(2 * alpha) + 144))/(315 * alphat**6)

        VVV = (-4 * np.pi**2 * np.sin(alpha) * ((alphat**2 - 2) * v**2 + (alphat**4 - alphat**2 + 1) * v**4 + 1) * (-b13*b23 * xi**2 * ((alphat**2 - 2) * v**2 + 2) + np.cos(alpha) * (xi**2 * ((alphat**2 - 2) * v**2 + 2) * (b22**2 + b23**2) - 12 * (b23 - b22)**2 * ((2 * alphat**2 - 1) * v**4 + v**2)) + 12 * (b13 - b11) * (b23 - b22) * v**2 * ((2 * alphat**2 - 1) * v**2 + 1)))/(315 * alphat**6)

        TTT = 0.

        return SSS000, SSS, VVV, TTT
        
        
        
#def _pickle_method(method):
#	func_name = method.im_func.__name__
#	obj = method.im_self
#	cls = method.im_class
#	return _unpickle_method, (func_name, obj, cls)
#
#def _unpickle_method(func_name, obj, cls):
#	for cls in cls.mro():
#		try:
#			func = cls.__dict__[func_name]
#		except KeyError:
#			pass
#		else:
#			break
#	return func.__get__(obj, cls)
#
#import copy_reg
#import types
#copy_reg.pickle(types.MethodType, _pickle_method, _unpickle_method)
#
#def correlator_loop(ktau, seed):
#	cpu = multiprocessing.current_process()._identity[0]-1
#	print "cpu = ", cpu, ", seed = ", seed
#	Str = Strings(S = StringParameters(num_rel = num_rel, s = seed))
#	return np.array(Str.calculate_correlator(ktau))
#
#
##from pathos.multiprocessing import ProcessingPool as Pool	
#import multiprocessing
#from functools import partial
#import os
#import sys
#import time
#import psutil
#
#grid = 100
#num_rel = 10
#
##S = Strings(S = StringParameters(num_rel = num_rel))
##ktau = S.get_ktau(grid, [1, 1, 1], 5e-3, 5e2)
#
#memory = 31
#system = psutil.virtual_memory()[1]/1e9
#memory_ratio = (system/memory)
#if memory_ratio <= int(memory_ratio):
#	ncpus = int(memory_ratio) - 1
#else:
#	ncpus = int(memory_ratio)
#
#if ncpus > multiprocessing.cpu_count() - 1:
#	ncpus = multiprocessing.cpu_count() - 1
#
#ncpus = 10
#ncpus = multiprocessing.cpu_count() - 1
#print "number of cpus = ", ncpus
#
#ncpus = #multiprocessing.cpu_count() - 1
#pool = multiprocessing.Pool(ncpus)
#loop = 10000
#
#
#load = None#'save/3pt_correlator_data_100.npy'
#
#if load is None:
#	S.correlators = np.zeros([4, grid, grid, grid])
#	l_init = 1
#else:
#	S.correlators = np.load(load)
#	l_init = int(load.replace('save/3pt_correlator_data_', '').replace('.npy', ''))
#
#
#tic = time.clock()
#l = l_init
#for job in pool.imap_unordered(partial(correlator_loop, ktau), range(loop)):
#	S.correlators += job
#	if l%100 == 0: #> l_init:
#		#os.remove('save/3pt_correlator_data_' + str(l-1) + '.npy')	
#		S.save('save/3pt_correlator_data_' + str(l), S.correlators)
#		threepointcorrelators_spline = S.correlator_interpolator(ktau, S.correlators/l)		
#		S.save('save/3pt_correlator_spline_' + str(int(l * num_rel)), threepointcorrelators_spline)
#	l += 1
#	toc = time.clock()
#	print l, toc - tic
#	
#
##threepointcorrelator = S.load('save/3ptcorrelator_test.npy',)
##S.plot_threepointcorrelator(threepointcorrelator, 100, [1, 1, 1], 1e-2, 1e2, 'TTT', 'test')

